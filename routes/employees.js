var express   = require('express')
var router    = express.Router();
var mongoose  = require('mongoose');
var path      = require('path');
var multer    = require('multer')

//===============================//
//        EMPLOYEE ROUTES        //
//===============================//


// Requiring employee Schema from models folder
var NewEmployee      = require("../models/employee");

//Using Multer to store Profile Image to Static folder uploads
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
      cb(null, path.resolve('./uploads'));
  },
  filename: function (req, file, cb) {
      cb(null, file.originalname);
  }
});

var upload = multer({
  storage: storage,
  limits: {
      fileSize: 1024 * 1024 * 5
  }
});


// @route   GET 
// @desc    Get all employees data
// @access  Public
router.get("/employees", function(req, res){
    NewEmployee.find({}, (err, allPro) =>{
      if(err){
        console.log(err);
        res.redirect("/");
      }
      else if(allPro.length == 0 ){
        res.render('employees/error_noEmployee');
        console.log("No Employe found")
      } else {
          res.render('employees/show_employee',{profile:allPro});
      }
    });
});


// @route   POST 
// @desc    POST employees data
// @access  Public
var profileImageUpload = upload.fields([{
  name:'image',
  maxCount: 1
}])
router.post("/employees",profileImageUpload,  (req, res) => {
  var first_name = req.body.first_name;
  var second_name = req.body.second_name;
  var score = req.body.score;
  var status = req.body.status;
  try {
    var image = req.files.image[0].filename;
} catch (error) {
    console.log("Some Document are not added");
}
  var newEmployee = {
    first_name: first_name,
    second_name:second_name,
    score: score,
    status: status,
    image:image
  };
  NewEmployee.create(newEmployee,  (err, new_employee) => {
      if (err) {
          console.log(err);
      } else {
          console.log(new_employee);
          req.flash("success_message","Employee created successfully");
          res.redirect("/employees");
      };
  });
});


// @route   GET 
// @desc    GEt data of employees with given ID(:id)
// @access  Public
router.get("/employees/:id", function(req, res){
  if(mongoose.Types.ObjectId.isValid(req.params.id)){
    NewEmployee.findById(req.params.id, (err, show) =>{
      if(err){
        console.log(err);
        res.redirect("/");
      } else {
          res.render('employees/show_employee_profile',{profile:show});
      }
    });
  }
  else{
    console.log("there is an error");
  }
  
});

// @route   PUT  "/employees/:id/edit"
// @desc    Update employee profile with ID(:id)
// @access  Public
router.put("/employees/:id/edit", (req, res) => {
  var id = req.params.id;
  if(req.body.profile.image==''){
    var data = {
      first_name: req.body.profile.first_name,
      second_name: req.body.profile.second_name,
      score: req.body.profile.score,
      status: req.body.profile.status
    }
  }
  else{
    var data =  req.body.profile;
  }
  console.log(req.body.profile.status);
  NewEmployee.findByIdAndUpdate(req.params.id,data, (err, allprofiles) => {
      if (err) {
          console.log(err)
      } else {
        
          res.redirect('/employees/' + req.params.id);
      };
  });
})

// @route   GET "/employees/:id/delete"
// @desc    Delete employee from database having ID):id
// @access  Public
router.get("/employees/:id/delete",  (req, res) => {
  var id = req.params.id;
  NewEmployee.findByIdAndDelete(id,function (err, dele) {
      if (err) {
          console.log(err)
      } else {
          res.redirect('/employees');
      };
  });
});

module.exports = router;