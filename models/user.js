var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Create user Schema
var UserSchema = new Schema({
    name: {
      type: String,
      required: true
    },
    phone_number: {
        type: Number,
        required: true
    }
  });

  module.exports = mongoose.model('users', UserSchema);
