var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Create Employee's Schema
var employeeSchema = new Schema({
      id: mongoose.Schema.Types.ObjectId,
      first_name: {
        type: String,
        required: true
  },
    second_name: {
        type: String,
        required: true
  },
      score: {
        type: Number,
        required: true
  },
      status: {
        type: String,
        required: true,
        default: 'off'
  },
    image:String
});

module.exports = mongoose.model("NewEmployee", employeeSchema);