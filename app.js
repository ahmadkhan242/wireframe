// Requiring all packages required in this project 
var express         = require('express'),
    app             = express(),
    mongoose        = require('mongoose'),
    bodyParser      = require('body-parser'),
    methodOverride  = require("method-override"),
    path            = require('path'),
    multer          = require('multer'),
    flash           = require('connect-flash'),
    session = require('express-session'),
    cookieParser = require('cookie-parser');

// Requiring user Schema from models folder
var NewUser      = require("./models/user");
var NewEmployee      = require("./models/employee");


// Requiring all routes from routes folder
var employeeRoutes   = require("./routes/employees");




//Connecting to Database
try{
    mongoose.connect('mongodb://localhost/wireframes_api');
}
catch(error){
    console.log("Database is not Connected, There is some error");
    console.log(error);
}



app.use('/uploads', express.static(__dirname + '/uploads'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.set("view engine", "ejs");
app.use(methodOverride("_method"));

app.set('views', path.join(__dirname, 'views'));
app.use('/assets',express.static('assets'));
app.use(flash());
app.use(cookieParser('secretString'));
app.use(session({cookie: { maxAge: 60000 }}));
app.use(function(req, res, next){
    res.locals.success_message = req.flash('success_message');
    res.locals.error_message = req.flash('error_message');
    next();
});


// @route   GET "/"
// @desc    Get the Home page(landing page)
// @access  Public
app.get("/", function(req, res){
        res.render('home');
});



// Using employeeRoutes routes from routes folder
app.use(employeeRoutes);



app.listen(process.env.PORT || 8000, () =>  {
    console.log("your server is on from port 8000");
});