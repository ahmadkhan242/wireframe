
var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

//To load Date when site is loaded
$(document).ready(function(){
var d = new Date();
  var year = d.getFullYear() % 100;
    document.getElementById("demo").innerHTML = d.getDate()+ "-" +months[d.getMonth()]+ "-" +year+" "+ d.getHours()+d.getMinutes()+ " hrs" ;
  });

// To Refresh date time every 5 min
window.setInterval(function(){
    var d = new Date();
    var year = d.getFullYear() % 100;
    document.getElementById("demo").innerHTML = d.getDate()+ "-" +months[d.getMonth()]+ "-" +year+" "+ d.getHours()+d.getMinutes()+ " hrs" ;
  }, 300000);

