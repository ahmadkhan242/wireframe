var modal = document.getElementById("myModal");

var btn = document.getElementById("myBtn");

var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
  modal.style.display = "block";
}

span.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

//For toggling Active and Inactive tag 
$(document).ready(function() {
  $('.slider').click(function() {
    $(this).siblings(".inactivetag").toggle();
    $(this).siblings(".activetag").toggle();
  });
});