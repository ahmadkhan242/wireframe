$(document).ready(function() {
    $(function() {
        $('#submit').attr('disabled', 'disabled');
        $("#submit").addClass("disabled");
    });
  // Did not include status because it is has default value and image is optional 
    $('input[type=text]').keyup(function() {   
        if ($('#first_name').val() !=''&&
        $('#second_name').val() != '' &&
        $('#score').val() != '' ) {
            $('#submit').removeAttr('disabled');
            $("#submit").removeClass("disabled");
        } else {
            $('#submit').attr('disabled', 'disabled');
            $("#submit").addClass("disabled");
        }
    });
        });